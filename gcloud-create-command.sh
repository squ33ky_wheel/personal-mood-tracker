#!/bin/sh

# setup instace.
gcloud compute instances create demo-instance --image-family debian-9 --image-project debian-cloud --machine-type g1-small --scopes "userinfo-email,cloud-platform" --metadata-from-file startup-script=instance-startup.sh --metadata BUCKET=pmt-01 --zone us-west1-b --tags http-server

# firewall rules
gcloud compute firewall-rules create default-allow-https-8443 --allow tcp:8443 --source-ranges 0.0.0.0/0 --target-tags http-server --description "Allow port 8443 access to http-server"

gcloud compute firewall-rules create default-allow-https-443 --allow tcp:443 --source-ranges 0.0.0.0/0 --target-tags http-server --description "Allow port 443 access to http-server"

gcloud compute firewall-rules create default-allow-https-8080 --allow tcp:8080 --source-ranges 0.0.0.0/0 --target-tags http-server --description "Allow port 8080 access to http-server"
