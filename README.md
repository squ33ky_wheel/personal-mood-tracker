# Personal Mood Tracker

Personal mood tracking is something that would be beneficial for keeping track of how one feels during the day, and around certain day to day events.

We're live at https://personalmoodtracker.com:8443/

## Motivation
See our [user story document](Data%20import) for information about what types of scenarios we are trying to solve.

## Deploying Spring Server
In your local windows machine, start `cmder` or some similar console emulator. Also make sure to have gradle 6.3 installed as described [here](https://gradle.org/install)

Run the following to get started from scratch:
```$sh
git clone git@gitlab.com:squ33ky_wheel/personal-mood-tracker.git
cd personal-mood-tracker
gradle bootRun
```

The `bootRun` gradle task will deploy the Spring Boot server locally.
Get started in your web browser at http://localhost:8080/ or http://localhost:8080/moodTracker


## Server Setup
We should setup our backend server to serve traffic. I suggest we use Java as our programming language, and the spring framework for managing our web server and architecture choices.

Web server quick start is here: https://spring.io/guides/gs/serving-web-content/

After that we should be able to create controllers and endpoints that handle operations of the front end application.

As far as where to setup the server, digital ocean sells "droplets"  (aka VMs) for $5 per month. Do you have google credits? I don't have any azure credits left. 
Eventually, we should probably setup on a VM in the cloud. For initial setup, running locally is a good start.

## Database Setup
We should use MongoDB. Here's a quick start for using a "managed" mongo db from a cloud service which is free. https://www.mongodb.com/blog/post/quick-start-java-and-mongodb--starting-and-setup
It should be simple to deploy mongo locally instead if you'd prefer.

Eventually, we'd need to setup mongo and a server virtual machine so that they can talk to each other.

Spring with Mondgo: https://spring.io/guides/gs/accessing-data-mongodb/

## Frontend setup
To begin, I think simply building a web-app would be easiest for testing and prototyping.
I am most familiar with the node.js + react stack. So I might go ahead and do that.

For the long term, Android has a framework that uses Java mostly for building apps. That would take a little while for me to understand and figure out. 
iOS requires a mac book for development and uses objective C
But once we have the web prototype it might be easier to port to an android and iOS version. 

We could also use an "app builder" to just generate us some apps. This might make it difficult to integrate with the lock screen since I suspect that's a very special/custom feature.

You can add the website to your phone home screen using firefox or chrome default feature. [HERE](https://www.howtogeek.com/196087/how-to-add-websites-to-the-home-screen-on-any-smartphone-or-tablet/)

## Additional references 
- https://docs.spring.io/spring-data/mongodb/docs/2.2.6.RELEASE/reference/html/#reference
- https://spring.io/guides/gs/accessing-data-mongodb/


## GCloud Setup References
I've used this tutorial to setup a compute resource hosting our server. [HERE](https://cloud.google.com/community/tutorials/kotlin-springboot-compute-engine)

See [instance-startup.sh](./instance-startup.sh) for how the instance is created

See [gcloud-create-command.sh](./gcloud-create-command.sh]) for some of the commands to run in terminal in order to setup the instance and firewall rule.
