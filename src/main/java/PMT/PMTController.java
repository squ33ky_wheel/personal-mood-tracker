package PMT;

import org.apache.logging.log4j.util.Strings;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;


@Controller
public class PMTController {

    @GetMapping("/PMTController")
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping("/{user}/moodTracker")
    public String moodForm(@PathVariable(value="user", required = true) String name) {
        return "responsiveButtons";
    }

    @PostMapping(value = "/{user}/submitEvent", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String submitEvent(@RequestBody(required = false)MultiValueMap<String, String> formData,
                              @PathVariable(value="user", required = true) String name) {
        if (name.equals("Alex") || name.equals("Pat")) {
            DBConnection.formEntry(formData, name);
            return "submitConfirmation";
        }
        else {
            return "shitNameBro";
        }
    }

    @PostMapping("/name")
    public  String nameRedirect(@RequestBody(required = true)MultiValueMap<String, String> formData){
        String userName = formData.getFirst("userName");
        if (userName.equals("Alex")) {
            return "redirect:/Alex/moodTracker";
        }
        else if (userName.equals("Pat")) {
            return "redirect:/Pat/moodTracker";
        }
        else{
            return "shitNameBro";
        }

    }
}


