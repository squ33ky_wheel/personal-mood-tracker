package PMT;

import com.mongodb.client.*;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.util.MultiValueMap;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.time.LocalDateTime;

public class DBConnection {

    private static String connectionString = "mongodb+srv://pksellsw:testtest@cluster0-ksebe.azure.mongodb.net/test?retryWrites=true&w=majority";

    public static List<Document> pullDBs() {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try (MongoClient mongoClient = MongoClients.create(connectionString)) {
            List<Document> databases = mongoClient.listDatabases().into(new ArrayList<>());
            databases.forEach(db -> System.out.println(db.toJson()));
            return databases;
        }
    }

    public static void formEntry(MultiValueMap<String, String> formData, String userName) {
        try(MongoClient mongoClient = MongoClients.create(connectionString)) {

            MongoDatabase databasePMT = mongoClient.getDatabase("PMT");
            MongoCollection<Document> userCollection = databasePMT.getCollection(userName);

            Document event = new Document("_id", new ObjectId());
            event.append("User", "Test")
                    .append("Time", LocalDateTime.now());
            formData.forEach((key, value) -> event.append(key, value.get(0)));
            userCollection.insertOne(event);
        }
    }

    public String serverString(){
        return connectionString;
    }
}


